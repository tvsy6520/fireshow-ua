const { Router } = require('express')
const db = require('../db')

const router = Router()

router.get('/show/:showId/programs/:programId/variants', async (req, res) => {
  try {
    const currentShow = req.params.showId
    const currentProgram = req.params.programId
    const query = `
        SELECT id_string, artist_count, is_vip, price, duration, playground, promo_url, poster_url
        FROM programs_variants
        WHERE show_id_string = "${currentShow}" AND program_id_string = "${currentProgram}"
        ORDER BY artist_count`
    const result = await db.query(query)

    if (!result) {
      return res.status(403).json({
        success: false,
        message: 'В данной программе нет вариантов!',
      })
    }

    return res.status(200).json(result)
  } catch (e) {
    return res.status(500).json(e)
  }
})

router.get(
  '/show/:showId/programs/:programId/variants/:variantId',
  async (req, res) => {
    try {
      const currentLang = req.query.lang
      const currentShow = req.params.showId
      const currentProgram = req.params.programId
      const currentVariant = req.params.variantId
      const query = `
        SELECT *
        FROM programs_variants
        WHERE id_string = "${currentVariant}" AND show_id_string = "${currentShow}" AND program_id_string = "${currentProgram}"`
      let result = await db.query(query)

      if (!result) {
        return res.status(403).json({
          success: false,
          message: 'В данной программе, нет такого варианта!',
        })
      }

      result = result.map((variant) => {
        return {
          id_string: variant.id_string,
          show: {
            id_string: variant.show_id_string,
          },
          program: {
            id_string: variant.program_id_string,
          },
          artist_count: variant.artist_count,
          is_vip: variant.is_vip,
          duration: variant.duration,
          playground: variant.playground,
          price: variant.price,
          avatar_url: variant.avatar_url,
          promo_url: variant.promo_url,
          poster_url: variant.poster_url,
          about: variant[`about_${currentLang}`],
          page_head: {
            title: variant[`page_title_${currentLang}`],
            desc: variant[`page_desc_${currentLang}`],
          },
        }
      })
      result = result[0]

      return res.status(200).json(result)
    } catch (e) {
      return res.status(500).json(e)
    }
  }
)

module.exports = router

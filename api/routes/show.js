const { Router } = require('express')
const db = require('../db')

const router = Router()

router.get('/show/all', async (req, res) => {
  try {
    const currentLang = req.query.lang
    const query = `
      SELECT id_string, name_${currentLang}, image_url
      FROM show_types`
    let result = await db.query(query)

    if (!result) {
      return res
        .status(403)
        .json({ success: false, message: 'Типы шоу не найдены!' })
    }

    const showTypes = []
    for (const show of result) {
      const query2 = `
        SELECT id_string
        FROM programs
        WHERE show_id_string = "${show.id_string}"
        LIMIT 1`
      const program = await db.query(query2)

      showTypes.push({
        id_string: show.id_string,
        program: {
          id_string: program[0]?.id_string,
        },
        name: show[`name_${currentLang}`],
        image_url: show.image_url,
      })
    }
    result = [...showTypes]

    return res.status(200).json(result)
  } catch (e) {
    return res.status(500).json(e)
  }
})

router.get('/show/individual/:showId', async (req, res) => {
  try {
    const currentLang = req.query.lang
    const currentShow = req.params.showId

    const query = `
      SELECT *
      FROM programs_individual
      WHERE show_id_string = "${currentShow}"`

    let result = await db.query(query)

    if (!result) {
      return res
        .status(403)
        .json({ success: false, message: 'Типы шоу не найдены!' })
    }

    result = result.map((show) => {
      return {
        show_id_string: show.show_id_string,
        about: show[`about_${currentLang}`],
        page_head: {
          title: show[`page_title_${currentLang}`],
          desc: show[`page_desc_${currentLang}`],
        },
      }
    })
    result = result[0]

    return res.status(200).json(result)
  } catch (e) {
    return res.status(500).json(e)
  }
})

module.exports = router

const { Router } = require('express')
const db = require('../db')

const router = Router()

router.get('/extracts', async (req, res) => {
  try {
    const query = `
      SELECT *
      FROM extracts`
    const result = await db.query(query)

    if (!result) {
      return res
        .status(403)
        .json({ success: false, message: 'Нет никаких дополнительных опций!' })
    }

    return res.status(200).json(result)
  } catch (e) {
    return res.status(500).json(e)
  }
})

router.get('/extracts/by-show/:showId', async (req, res) => {
  try {
    const currentLang = req.query.lang
    const currentShow = req.params.showId
    const query = `
      SELECT *
      FROM extracts`
    let result = await db.query(query)

    result = result.filter((extract) => {
      const canUseList = extract.can_use.split(';')
      const findExtractInCurrentShow = canUseList.find(
        (show) => show === currentShow
      )

      return !!findExtractInCurrentShow
    })

    if (!result) {
      return res.status(403).json({
        success: false,
        message: 'В данном типе шоу, нет дополнительных опций!',
      })
    }

    result = result.map((extract) => {
      return {
        id_string: extract.id_string,
        name: extract[`name_${currentLang}`],
        avatar_url: extract.avatar_url,
        promo_url: extract.promo_url,
        price: extract.price,
        about: extract[`about_${currentLang}`],
      }
    })

    return res.status(200).json(result)
  } catch (e) {
    return res.status(500).json(e)
  }
})
router.get('/extracts/by-individual-show/:showId', async (req, res) => {
  try {
    const currentLang = req.query.lang
    const currentShow = req.params.showId
    const query = `
      SELECT *
      FROM extracts_individual`
    let result = await db.query(query)

    if (!result) {
      return res.status(403).json({
        success: false,
        message: 'В данном типе шоу, нет дополнительных опций!',
      })
    }

    result = result.filter((extract) => {
      const canUseList = extract.can_use.split(';')
      const findExtractInCurrentShow = canUseList.find(
        (show) => show === currentShow
      )

      return !!findExtractInCurrentShow
    })

    result = [
      ...result.map((extract) => {
        return {
          id_string: extract.id_string,
          title: extract[`title_${currentLang}`],
          about_short: extract[`about_short_${currentLang}`],
          promo_url: extract.promo_url,
          poster_url: extract.poster_url,
          questions: extract[`questions_${currentLang}`],
          page_head: {
            title: extract[`page_title_${currentLang}`],
            desc: extract[`page_desc_${currentLang}`],
          },
        }
      }),
    ]

    return res.status(200).json(result)
  } catch (e) {
    return res.status(500).json(e)
  }
})

router.get('/extracts/:extractId', async (req, res) => {
  try {
    const currentLang = req.query.lang
    const currentExtract = req.params.extractId
    const query = `
      SELECT *
      FROM extracts
      WHERE id_string = "${currentExtract}"`
    let result = await db.query(query)

    if (!result) {
      return res.status(403).json({
        success: false,
        message: 'Такой дополнительной опции не существует!',
      })
    }

    result = result.map((extract) => {
      return {
        id_string: extract.id_string,
        name: extract[`name_${currentLang}`],
        avatar_url: extract.avatar_url,
        price: extract.price,
        about: extract[`about_${currentLang}`],
        page_head: {
          title: extract[`page_title_${currentLang}`],
          desc: extract[`page_desc_${currentLang}`],
        },
      }
    })
    result = result[0]

    return res.status(200).json(result)
  } catch (e) {
    return res.status(500).json(e)
  }
})
router.get('/extracts/individual/:extractId', async (req, res) => {
  try {
    const currentLang = req.query.lang
    const currentExtract = req.params.extractId
    const query = `
      SELECT *
      FROM extracts_individual
      WHERE id_string = "${currentExtract}"`

    let result = await db.query(query)

    if (!result) {
      return res.status(403).json({
        success: false,
        message: 'Такой дополнительной опции не существует!',
      })
    }

    result = result.map((extract) => {
      return {
        id_string: extract.id_string,
        title: extract[`title_${currentLang}`],
        about: extract[`about_${currentLang}`],
        questions: extract[`questions_${currentLang}`],
        promo_url: extract.promo_url,
        poster_url: extract.poster_url,
        gallery_urls: extract.gallery_urls.split(';'),
        page_head: {
          title: extract[`page_title_${currentLang}`],
          desc: extract[`page_desc_${currentLang}`],
        },
      }
    })
    result = result[0]

    return res.status(200).json(result)
  } catch (e) {
    return res.status(500).json(e)
  }
})

module.exports = router

const { Router } = require('express')
const db = require('../db')

const router = Router()

router.get('/show/:showId/programs', async (req, res) => {
  try {
    const currentShow = req.params.showId
    const query = `
      SELECT id_string, show_id_string, name, color_primary, color_secondary, color_colorful
      FROM programs
      WHERE show_id_string = "${currentShow}"`
    let result = await db.query(query)

    if (!result) {
      return res
        .status(403)
        .json({ success: false, message: 'В данном типе шоу, программ нет!' })
    }

    result = result.map((program) => {
      return {
        id_string: program.id_string,
        show: {
          id_string: program.show_id_string,
        },
        name: program.name,
        color: {
          primary: program.color_primary,
          secondary: program.color_secondary,
          colorful: program.color_colorful,
        },
      }
    })

    return res.status(200).json(result)
  } catch (e) {
    console.error(e)
    return res.status(500).json(e)
  }
})

router.get('/show/:showId/programs/:programId', async (req, res) => {
  try {
    const currentLang = req.query.lang
    const currentShow = req.params.showId
    const currentProgram = req.params.programId
    const query = `
      SELECT id_string, show_id_string, name, promo_url, poster_url, gallery_urls, about_${currentLang}, page_title_${currentLang}, page_desc_${currentLang}
      FROM programs
      WHERE id_string = "${currentProgram}" AND show_id_string = "${currentShow}"`
    let result = await db.query(query)

    if (!result) {
      return res.status(403).json({
        success: false,
        message: 'В данном типе шоу, нет такой программы!',
      })
    }

    result = result.map((program) => {
      return {
        id_string: program.id_string,
        show: {
          id_string: program.show_id_string,
        },
        name: program.name,
        promo_url: program.promo_url,
        poster_url: program.poster_url,
        gallery_urls: program.gallery_urls.split(';'),
        about: program[`about_${currentLang}`],
        page_head: {
          title: program[`page_title_${currentLang}`],
          desc: program[`page_desc_${currentLang}`],
        },
      }
    })
    result = result[0]

    return res.status(200).json(result)
  } catch (e) {
    console.error(e)
    return res.status(500).json(e)
  }
})

router.get('/show/:showId/programs/:programId/colors', async (req, res) => {
  try {
    const currentShow = req.params.showId
    const currentProgram = req.params.programId
    const query = `
        SELECT color_primary, color_secondary, color_colorful
        FROM programs
        WHERE id_string = "${currentProgram}" AND show_id_string = "${currentShow}"`
    let result = await db.query(query)

    if (!result) {
      return res.status(403).json({
        success: false,
        message: 'В данном типе шоу, нет такой программы!',
      })
    }

    result = result.map((program) => {
      return {
        primary: program.color_primary,
        secondary: program.color_secondary,
        colorful: program.color_colorful,
      }
    })
    result = result[0]

    return res.status(200).json(result)
  } catch (e) {
    return res.status(500).json(e)
  }
})

module.exports = router

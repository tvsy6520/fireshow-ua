const { Router } = require('express')
const nodemailer = require('nodemailer')

const router = Router()

const transporter = nodemailer.createTransport({
  host: 'smtp.yandex.ru',
  port: 465,
  secure: true, // true for 465, false for other ports
  auth: {
    user: 'info@fireshow.ua', // generated ethereal user
    pass: '-wfhg_er)fwj', // generated ethereal password
  },
})

router.post('/send-mail', (req, res) => {
  try {
    let isResult
    const formData = req.body
    console.log('formData node: ', formData)

    const mailOptions = {
      from: 'info@fireshow.ua',
      to: 'fire.kiev.scorpius@gmail.com',
      subject: 'Оформили ЗАКАЗ на шоу!',
      html: `
        <h1>Имя заказчика: ${formData.name}</h1>
        <p>Телефон: ${formData.phone}</p>
        <p>Почта: ${formData.email ? formData.email : ''}</p>
        <p>Адресс: ${formData.address ? formData.address : ''}</p>
        <p>Дата: ${formData.date ? formData.date : ''}</p>
        <p>Время: ${formData.time ? formData.time : ''}</p>
        <hr>
        <p>Шоу: ${formData.show ? formData.show : ''}</p>
        <p>Программа: ${formData.program ? formData.program : ''}</p>
        <p>Количество артистов: ${formData.artists ? formData.artists : ''}</p>
        <p>Доп. услуги: ${formData.extracts ? formData.extracts : ''}</p>
        <p>Общая стоимость: ${formData.price ? formData.price : ''}</p>
        <p>Комментарий к заказу: ${formData.comment ? formData.comment : ''}</p>
      `,
    }

    transporter.sendMail(mailOptions, function (error) {
      isResult = !error

      transporter.close()
    })

    if (isResult) {
      console.log('ok good luck')
    }
    return res.status(200).json({ success: true, message: 'Усё гуд!' })
  } catch (e) {
    console.error(e)
  }
})

module.exports = router

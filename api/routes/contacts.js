const { Router } = require('express')
const db = require('../db')

const router = Router()

router.get('/contacts', async (req, res) => {
  try {
    let result = await db.query('SELECT * FROM contacts')
    result = result[0]

    const phoneList = result.phones.split(';')
    const resultObject = {
      phones: phoneList,
      instagram: result.instagram_url,
      facebook: result.facebook_url,
    }

    return res.status(200).json(resultObject)
  } catch (e) {
    return res.status(500).json(e)
  }
})

module.exports = router

const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const multer = require('multer')
const upload = multer()

// Create express instance
const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(morgan('combined'))
app.use(upload.array())
app.use(express.static('public'))

// Require API routes
const show = require('./routes/show')
const contacts = require('./routes/contacts')
const programs = require('./routes/programs')
const variants = require('./routes/variants')
const extracts = require('./routes/extracts')
const order = require('./routes/order')

// Import API Routes
app.use(show)
app.use(contacts)
app.use(programs)
app.use(variants)
app.use(extracts)
app.use(order)

// Export express app
module.exports = app

// Start standalone server if directly running
if (require.main === module) {
  const port = process.env.DB_PORT || 3001
  app.listen(port, () => {
    // eslint-disable-next-line no-console
    console.log(`API server listening on port ${port}`)
  })
}

const env = process.env

const config = {
  db: {
    host: env.DB_HOST || 'localhost',
    user: env.DB_USERNAME || 'root',
    password: env.DB_PASSWORD || 'root',
    database: env.DB_NAME || 'fireshow_v2',
  },
  listPerPage: env.LIST_PER_PAGE || 10,
}

module.exports = config

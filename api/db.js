'use strict'

const mysql = require('mysql2/promise')
const config = require('./config')

class Connection {
  constructor() {
    this.connection = undefined
  }

  async init() {
    this.connection = await mysql.createPool(config.db)
  }

  async query(sql, params) {
    if (!this.connection) {
      console.log('What?')
      this.connection = await mysql.createPool(config.db)
      const [results] = await this.connection.query(sql, params)
      return results
    } else {
      const [results] = await this.connection.query(sql, params)
      return results
    }
  }
}

// eslint-disable-next-line no-new
const mainConnect = new Connection()
mainConnect.init().then(() => {})

/* async function query(sql, params) {
  const connection = await mysql.createConnection(config.db)
  const [results] = await connection.execute(sql, params)

  return results
} */

module.exports = { query: mainConnect.query }

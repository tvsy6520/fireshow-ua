export const state = () => ({
  selectedExtracts: [],
})

export const getters = {
  SELECTED_EXTRACTS: (state) => state.selectedExtracts,
}

export const mutations = {
  SELECT_EXTRACT: (state, extractData) => {
    state.selectedExtracts = [...state.selectedExtracts, [extractData]]
  },
  DELETE_EXTRACT: (state, extractId) => {
    state.selectedExtracts = [
      ...state.selectedExtracts.filter((extract) => {
        return extract.id_string !== extractId
      }),
    ]
  },
}

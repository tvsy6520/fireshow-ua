export default {
  /**
   * Loader
   */
  loading: { color: '#e4667b', height: '5px' },
  generate: {
    fallback: true,
  },

  ssr: true,

  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',

  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'fireshow',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Roboto:wght@400;700&display=swap',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~assets/basic.scss'],

  styleResources: {
    scss: [
      '~/assets/_fonts.scss',
      '~/assets/_vars.scss',
      '~/assets/_classes.scss',
      '~/assets/basic.scss',
    ],
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // https://vuelidate.js.org/#getting-started
    { src: '~/plugins/vuelidate.js' },
    // https://github.com/pexea12/vue-image-lightbox
    { src: '~/plugins/lightbox.js', mode: 'client' },
    // { src: '~/plugins/splide.client.js', mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://github.com/nuxt-community/fontawesome-module#readme
    '@nuxtjs/fontawesome',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://github.com/nuxt-community/style-resources-module#readme
    '@nuxtjs/style-resources',
    // https://i18n.nuxtjs.org/setup
    'nuxt-i18n',
    // https://github.com/microcipcip/cookie-universal/tree/master/packages/cookie-universal-nuxt#readme
    ['cookie-universal-nuxt', { alias: 'cookiz', parseJSON: false }],
    // https://github.com/nuxt-community/gtm-module
    '@nuxtjs/gtm',
  ],

  // Nuxt server middleware: https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-servermiddleware
  serverMiddleware: {
    '/api': '~/api',
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: process.env.API_URL,
    debug: true,
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'uk',
    },
  },

  /*
   ** i18n module configuration
   ** See https://i18n.nuxtjs.org
   */
  i18n: {
    locales: [
      { code: 'en', iso: 'en-US' },
      { code: 'ru', iso: 'ru-RU' },
      { code: 'uk', iso: 'uk-UK' },
    ],
    defaultLocale: 'uk',
    vueI18nLoader: true,
    vueI18n: {
      fallbackLocale: 'uk',
    },
  },

  gtm: {
    id: process.env.GOOGLE_TAG_MANAGER_ID,
  },
  publicRuntimeConfig: {
    gtm: {
      id: process.env.GOOGLE_TAG_MANAGER_ID,
    },
  },

  // https://github.com/nuxt-community/fontawesome-module#readme
  fontawesome: {
    component: 'fa',
    suffix: true,
    icons: {
      solid: true,
      regular: true,
      brands: true,
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}

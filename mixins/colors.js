export default {
  data() {
    return {
      colorVars: null,
    }
  },
  methods: {
    setColors() {
      if (document && this.colorVars) {
        document.documentElement.style.setProperty(
          '--program-color-primary',
          this.colorVars.primary
        )
        document.documentElement.style.setProperty(
          '--program-color-secondary',
          this.colorVars.secondary
        )
        document.documentElement.style.setProperty(
          '--program-color-colorful',
          this.colorVars.colorful
        )
      }
    },
  },
  async mounted() {
    this.colorVars = await this.$axios.$get(
      `/api/show/${this.$route.params.show}/programs/${this.$route.params.program}/colors`
    )
    this.setColors()
  },
}

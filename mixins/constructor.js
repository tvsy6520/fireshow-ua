export default {
  data() {
    return {
      contacts: {},
    }
  },
  methods: {
    goBack() {
      this.$router.go(-1)
    },
    checkSavedExtracts(to, from) {
      const isSavedExtracts = this.$cookiz.get('saved-extracts')
      if (isSavedExtracts && to.params?.show !== from.params?.show) {
        this.$cookiz.remove('saved-extracts')
      }
    },
    async checkShow() {
      if (this.$route.params.show) {
        const showTypes = this.showTypes
          ? this.showTypes
          : await this.$axios.$get('/api/show/all', {
              params: {
                lang: this.$i18n.localeProperties.code,
              },
            })
        const findedShow = showTypes.find(
          (show) => show.id_string === this.$route.params.show
        )

        if (!findedShow) {
          return this.$nuxt.error({
            statusCode: 404,
            message: 'show',
          })
        }
      }
    },
    async checkProgram() {
      if (this.$route.params.program) {
        const currentProgram = this.currentProgram
          ? this.currentProgram
          : await this.$axios.$get(
              `/api/show/${this.$route.params.show}/programs/${this.$route.params.program}`,
              {
                params: {
                  lang: this.$i18n.localeProperties.code,
                },
              }
            )

        if (!currentProgram) {
          return this.$nuxt.error({
            statusCode: 404,
            message: 'program',
          })
        }
      }
    },
    async checkVariant() {
      if (this.$route.params.variant) {
        const currentVariant = this.currentVariant
          ? this.currentVariant
          : await this.$axios.$get(
              `/api/show/${this.$route.params.show}/programs/${this.$route.params.program}/variants/${this.$route.params.variant}`,
              {
                params: {
                  lang: this.$i18n.localeProperties.code,
                },
              }
            )

        if (!currentVariant) {
          return this.$nuxt.error({
            statusCode: 404,
            message: 'variant',
          })
        }
      }
    },
  },
  beforeRouteEnter(to, from, next) {
    next((vm) => {
      vm.checkSavedExtracts(to, from)
      vm.checkShow()
      vm.checkProgram()
      vm.checkVariant()
    })
  },
  async mounted() {
    this.contacts = await this.$axios.$get('/api/contacts')
  },
}

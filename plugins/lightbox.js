import Vue from 'vue'
import VueLazyLoad from 'vue-lazyload'
import VueSilentbox from 'vue-silentbox'

Vue.use(VueLazyLoad)
Vue.use(VueSilentbox)
